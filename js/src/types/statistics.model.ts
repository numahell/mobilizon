export interface IStatistics {
  numberOfUsers: number;
  numberOfEvents: number;
  numberOfComments: number;
}
